class Population {
  mutationRate;
  population=[];
  matingPool = [];
  target=[];
  generetions = 0;
  finished;
  perfectScore=0;

  constructor(target, mutation, pop){
    this.target = target;
    this.mutationRate = mutation;
    for(var i=0; i<pop;i++){
      this.population.push(new DNA(target.length))
    }
    this.calcFitness();
    this.finished = false;
    this.generations = 0;
    this.perfectScore = 1;
  }

  calcFitness(){
    for(var i=0; i<this.population.length;i++){
      this.population[i].resultFitness(target)
    }
  }

  naturalSelection(){
    this.matingPool = [];
    let maxFitness = 0;

    for(var i=0; i < this.population.length;i++){
      if(this.population[i].fitness > maxFitness){
        maxFitness = this.population[i].fitness
      }
    }

    for(var i=0; i<this.population.length;i++){
      let fitness = map(this.population[i].fitness, 0, maxFitness, 0, 1);
      let n = parseInt(fitness * 100);
      for(var j=0; j<n; j++){
        this.matingPool.push(this.population[i]);
      }
    }

  }

  generate(){
    for(var i=0; i<this.population.length;i++){
      let a = parseInt(random(this.matingPool.length));
      let b = parseInt(random(this.matingPool.length));
      let partnerA = this.matingPool[a];
      let partnerB = this.matingPool[b];
      let child = partnerA.crossover(partnerB);
      child.mutate(mutationRate);
      this.population[i] = child;
    }
    this.generations++;
  }

  getBest(){
    let worldScore = 0.0;
    let index = 0;
    print(this.population.length)
    for(var i=0; i<this.population.length;i++){
      if(this.population[i].fitness > worldScore){
        index = i;
        worldScore = this.population[i].fitness;
      }
    }
    print(worldScore);
    if(worldScore == this.perfectScore){
      this.finished=true;
    }
    return this.population[index].getParam();

  }

}
