var target = [];
var popmax = 0;
var mutationRate = 0.0;

var food = [];
var poison = [];

var vehicles = [];

frames = 0;

var debug;



function setup(){
  createCanvas(1280, 720);
  target = [4, -4, 10, 1];
  popmax = 50;
  mutationRate = 0.01;

  population = new Population(target, mutationRate, popmax);

  for (var a = 0; a < 300; a++) {
      var x = random(width);
      var y = random(height);
      food.push(createVector(x, y));
  }
  for (var a = 0; a < 60; a++) {
      var x = random(width);
      var y = random(height);
      poison.push(createVector(x, y));
  }
  for(var i=0;i<population.population.length;i++){
      var x = random(width);
      var y = random(height);
      vehicles.push(new Vehicle(x, y, population.population[i]));
  }
  debug=createCheckbox();

}

function draw(){
  background(44);

  if(frames==180 && !population.finished){
    frames=0;
    vehicles = [];
    food =[];
    poison=[];
    population.naturalSelection();

    population.generate();

    population.calcFitness();

    print(population.getBest());

    for(var i=0;i<population.population.length;i++){
        var x = random(width);
        var y = random(height);
        if(population.population[i].genes[0] ==4  && population.population[i].genes[1] ==-4 && population.population[i].genes[2] ==10 &&population.population[i].genes[3] ==1){
          vehicles.push(new Vehicle(x, y, population.population[i], true));
        }
        vehicles.push(new Vehicle(x, y, population.population[i],false));
    }

    for (var a = 0; a < 300; a++) {
        var x = random(width);
        var y = random(height);
        food.push(createVector(x, y));
    }

    for (var a = 0; a < 60; a++) {
        var x = random(width);
        var y = random(height);
        poison.push(createVector(x, y));
    }

  }


  if(random(1) < 0.8 && food.length < 350 ){
      var x = random(width);
      var y = random(height);
      food.push(createVector(x, y));
  }
  if(random(1) < 0.10 && poison.length< 70 ){
      var x = random(width);
      var y = random(height);
      poison.push(createVector(x, y));
  }

  stroke(0);
  for (var a = 0; a < food.length; a++) {
      fill(0, 255, 0);
      ellipse(food[a].x, food[a].y, 8, 8);
  }
  for (var a = 0; a < poison.length; a++) {
      fill(255, 0, 0);
      ellipse(poison[a].x, poison[a].y, 8, 8);
  }

  for (var a = vehicles.length - 1; a >=0; a--) {
      vehicles[a].behaviors(food, poison);
      vehicles[a].update();
      vehicles[a].boundaries();
      vehicles[a].display();
  }

  frames++;

}
