// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Seeking "vehicle" follows the target position

// Implements Craig Reynold's autonomous steering behaviors
// One vehicle "seeks"
// See: http://www.red3d.com/cwr/

//Vehicle vehicle;
//alert(width);

var vehicles = [];
var food = [];
var poison = [];

function setup() {
    createCanvas(800, 600);
    for (var a = 0; a < 100; a++) {
        var x = random(width);
        var y = random(height);
        food.push(createVector(x, y));
    }
    for (var a = 0; a < 20; a++) {
        var x = random(width);
        var y = random(height);
        poison.push(createVector(x, y));
    }
    for (var a = 0; a < 1; a++) {
        var x = random(width);
        var y = random(height);
        vehicles[a] = new Vehicle(x, y);
    }
}

function mouseDragged(){
    vehicles.push(new Vehicle(mouseX, mouseY));
}

function draw() {
    background(51);

    if(random(1) < 0.1 && food.length < 101 ){
        var x = random(width);
        var y = random(height);
        food.push(createVector(x, y));
    }
    if(random(1) < 0.05 && poison.length< 31 ){
        var x = random(width);
        var y = random(height);
        poison.push(createVector(x, y));
    }

    //PVector target = new PVector(mouseX, mouseY);
    //target = createVector(mouseX, mouseY);

    // Draw an ellipse at the target position
    //fill(200);

    //strokeWeight(2);
    //ellipse(target.x, target.y, 48, 48);
    stroke(0);
    for (var a = 0; a < food.length; a++) {
        fill(0, 255, 0);
        ellipse(food[a].x, food[a].y, 8, 8);
    }
    for (var a = 0; a < poison.length; a++) {
        fill(255, 0, 0);
        ellipse(poison[a].x, poison[a].y, 8, 8);
    }
    for (var a = vehicles.length - 1; a >=0; a--) {
        vehicles[a].behaviors(food, poison);
        vehicles[a].update();
        vehicles[a].boundaries();
        vehicles[a].display();

        // var newVehicle = vehicles[a].clone();
        // if(newVehicle!= null){
        //     vehicles.push(newVehicle);
        // }

        if (vehicles[a].dead()){
            var newVehicle = new Vehicle(vehicles[a].position.x,vehicles[a].position.y,vehicles[a].dna);
            vehicles.push(newVehicle);
            var x = vehicles[a].position.x;
            var y = vehicles[a].position.x;
            food.push(createVector(x,y));
            vehicles.splice(a, 1);
        }
    }

    // Call the appropriate steering behaviors for our agents


    //vehicle.seek(target);
    //vehicle.eat(food);
    //vehicle.eat(poison);
}
