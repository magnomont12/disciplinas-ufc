class Vehicle {

  position;
  velocity;
  acceleration;
  r;
  maxforce;    // Maximum steering force
  maxspeed;    // Maximum speed
  health;

  constructor(x, y, dna) {
    this.acceleration = createVector(0, 0);
    this.velocity = createVector(0, -2);
    this.position = createVector(x, y);
    this.r = 4;
    this.maxspeed = 5;
    this.maxforce = 0.1;
    this.dna = [];
    //Food weight
    this.dna[0] = dna.genes[0];
    //Food weight
    this.dna[1] = dna.genes[1];
    //Food perception
    this.dna[2] = dna.genes[2];
    //Poison perception
    this.dna[3] = dna.genes[3];
    this.health = 1;
  }

  // Method to update position
  update() {
    this.health -= 0.01;
    // Update velocity
    this.velocity.add(this.acceleration);
    // Limit speed
    this.velocity.limit(this.maxspeed);
    this.position.add(this.velocity);
    // Reset accelerationelertion to 0 each cycle
    this.acceleration.mult(0);
  }

  applyForce(force) {
    // We could add mass here if we want A = F / M
    this.acceleration.add(force);
  }

  behaviors(good, bad) {
    var steerG = this.eat(good, 0.1, this.dna[2]);
    var steerB = this.eat(bad, -0.5, this.dna[3]);

    steerG.mult(this.dna[0]);
    steerB.mult(this.dna[1]);

    this.applyForce(steerG);
    this.applyForce(steerB);
  }

  eat(list, nutrition, perception) {
    var record = Infinity;
    var closest = null;
    for (var a = list.length-1; a>=0 ; a--) {
      //var d = dist(this.position.x, this.position.y, list[a].x, list[a].y);
      var d = this.position.dist(list[a]);
      if (d < this.maxspeed) {
        list.splice(a, 1);
        this.health += nutrition;
      }else{
        if (d < record && d < perception) {
          record = d;
          closest = list[a];
        }
      }
    }
    //This is the moment eating
    if (closest != null) {
      return this.seek(closest);
    }
    return createVector(0, 0);
  }

  // A method that calculates a steering force towards a target
  // STEER = DESIRED MINUS VELOCITY
  seek(target) {
    let desired = p5.Vector.sub(target, this.position);  // A vector pointing from the position to the target

    // Scale to maximum speed
    desired.setMag(this.maxspeed);

    // Steering = Desired minus velocity
    let steer = p5.Vector.sub(desired, this.velocity);
    steer.limit(this.maxforce);  // Limit to maximum steering force

    return steer;
    //this.applyForce(steer);
  }

  dead(){
    return (this.health < 0)
  }

  display() {
    // Draw a triangle rotated in the direction of velocity
    let theta = this.velocity.heading() + PI / 2;

    push();
    translate(this.position.x, this.position.y);
    rotate(theta);
    strokeWeight(3);
    stroke(0, 255, 0);
    noFill();
    strokeWeight(2);
    line(0, 0, 0, -this.dna[0] * 15);
    ellipse(0,0,this.dna[2]*2);
    stroke(255, 0, 0);
    line(0, 0, 0, -this.dna[1] * 15);
    ellipse(0,0,this.dna[3]*2);

    var gr = color(0,255,0);
    var rd = color(255,0,0);
    var col = lerpColor(rd, gr, this.health);

    fill(col);
    stroke(200);
    strokeWeight(1);
    beginShape();
    vertex(0, -this.r * 2);
    vertex(-this.r, this.r * 2);
    vertex(this.r, this.r * 2);
    endShape(CLOSE);
    pop();
  }

  boundaries() {

    var d = 25;
    let desired = null;

    if (this.position.x < d) {
      desired = createVector(this.maxspeed, this.velocity.y);
    } else if (this.position.x > width - d) {
      desired = createVector(-this.maxspeed, this.velocity.y);
    }

    if (this.position.y < d) {
      desired = createVector(this.velocity.x, this.maxspeed);
    } else if (this.position.y > height - d) {
      desired = createVector(this.velocity.x, -this.maxspeed);
    }

    if (desired !== null) {
      desired.normalize();
      desired.mult(this.maxspeed);
      let steer = p5.Vector.sub(desired, this.velocity);
      steer.limit(this.maxforce);
      this.applyForce(steer);
    }
  }

}
