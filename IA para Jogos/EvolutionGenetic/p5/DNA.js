class DNA{
  genes = [];
  fitness;

  constructor(num){
    for(var i=0; i<num; i++){
      this.genes.push(parseInt(random(11, -5)));
    }
  }


  resultFitness(listTarget){
    let score = 0;
    for(var i=0; i<this.genes.length;i++){
      if (this.genes[i] == listTarget[i]){
        score++;
      }
    }
    this.fitness = score/listTarget.length;
    return this.fitness;
  }

  crossover(partner){
    let child = new DNA(this.genes.length);
    let midpoint = parseInt(random(this.genes.length));

    for(var i=0; i<this.genes.length;i++){
      if(i > midpoint){
        child.genes[i] = this.genes[i];
      }else{
        child.genes[i]= partner.genes[i];
      }
    }
    return child;
  }

  mutate(mutationRate){
    for(var i=0; i<this.genes.length;i++){
      if(random(1) < mutationRate){
        this.genes[i] = parseInt(random(11,-5));
      }
    }
  }

  getParam(){
    return this.genes.slice();
  }
}
